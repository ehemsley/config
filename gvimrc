" GUI vim options

" take up full screen
set columns=200
set lines=200

set cursorline
set cursorcolumn

set guioptions-=t
set guioptions-=T
set guioptions-=r

colors twilight
