export MANPATH=/opt/local/share/man:$MANPATH
export CDPATH=~/projects:~/
export RUBYOPT='rubygems'
export GREP_OPTIONS='--color=auto' 
export GREP_COLOR='3;33'
export EDITOR='vim'
export TERM=xterm-color
export LSCOLORS=gxfxcxdxbxegedabagacad
export CLICOLOR=1
export PROJECTS=$HOME/projects

export NODE_PATH=/usr/local/lib/node:/usr/local/lib/node_modules

export CLICOLOR=1 # turns on colors
export LSCOLORS=gxfxcxdxbxegedabagacad
export TERM=xterm-color

export GREP_COLOR='3;33'
export GREP_OPTIONS='--color=auto' 

export RUBYOPT='rubygems' # ruby always requires rubygems
